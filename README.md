# BSPWM Setup
    
## Tutorial for bspwm-wm settings:
- Background handled by nitrogen
- Gtk3 theme handled by lxappearance-gtk3
- Filebrowser = Thunar
- Default Terminal-Emulator = Xfce4-terminal
- Text-Editor = xed
- Application Launcher = Rofi


![bspwm1](https://raw.githubusercontent.com/theCode-Breaker/bspwm-dotfiles/main/bspwm.png) 
![bspwm2](https://raw.githubusercontent.com/theCode-Breaker/bspwm-dotfiles/main/bspwm2.png) 
![bspwm3](https://raw.githubusercontent.com/theCode-Breaker/bspwm-dotfiles/main/bspwm3.png)

## [Keybindings](https://raw.githubusercontent.com/theCode-Breaker/bspwm-dotfiles/main/.config/sxhkd/sxhkdrc)

## **NOTE**- 
- Some packages may be missing so get them manually from AUR
- I am using [Nordic](https://github.com/EliverLara/Nordic) theme get it manually
- Use whatever icons you like
     
**ENJOY**
